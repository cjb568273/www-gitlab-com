---
layout: markdown_page
title: "Category Direction - Code Review Workflow"
description: "The Code Review Workflow strategy page belongs to the Code Review group of the Create stage. Learn more here!"
---

- TOC
{:toc}

## Code Review Workflow

| | |
| --- | --- |
| Stage | [Create](/direction/create/) |
| Maturity | [Loveable](/direction/maturity/) |
| Content Last Reviewed | `2023-01-26` |

## Introduction and how you can help

Thanks for visiting this direction page on Code Review in GitLab. This page belongs to the [Code Review](/handbook/product/categories/#code-review-group) group of the Create stage and is maintained by Kai Armstrong ([E-Mail](mailto:karmstrong@gitlab.com)).

This direction is constantly evolving and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=Category%3ACode%20Review%20Workflow) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=Category:Code+Review+Workflow) on this page. Sharing your feedback directly on GitLab.com or submitting a merge request to this page are the best ways to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or [schedule a video call](https://calendly.com/gitlabkai). If you're a GitLab user and have direct knowledge of your need for Code Review, we'd especially love to hear from you.

## Strategy and Themes
<!-- Describe your category. Capture the main problems to be solved in market (themes). Describe how you intend to solve these with GitLab (strategy). Provide enough context that someone unfamiliar with the details of the category can understand what is being discussed. -->

Code Review is an essential activity of software development. It ensures that contributions to a project maintain and improve code quality and security, and is an avenue of mentorship and feedback for engineers. It can also be one of the most time consuming activities in the software development process.

**GitLab's guiding principle for Code Review is:** Reviewing code is an activity that ultimately improves the resulting product, by improving the quality of the code while optimizing for the speed at which that code is delivered.

The Code Review process begins with authors proposing changes to an existing project via a change proposal. Once they've proposed the changes they need to request feedback from peers (Developers, Designers, Security and Operations Teams, Product Managers, etc) and then respond to that feedback. Ultimately, a merge request needs to be approved and then merged for the Code Review process to be completed for a given changeset.

<a name="vision"></a>
**GitLab's vision for code review is a place where:**

- **changes can be discussed,**
- **developers can be mentored,**
- **knowledge can be shared,**
- **defects identified, and**
- **contributions delivered.**

In GitLab, Code Review takes place in the [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/). **GitLab should make these tasks efficient and easy, so that velocity and code quality and security both increase while allowing for future iterations.**

The code review process involves at least two roles (author, and reviewer) but may involve many people,
who work together to achieve code quality standards and mentor the author.
Furthermore, many reviewers are often not Developers.
Reviewers may be Developers, Product Designers, Product Managers, Technical Writers, Security Engineers and more.

In support of GitLab's [vision](#vision) for code review, areas of interest and improvement can be organized by the following themes:

- **Ease of use** influences whether users choose to use the GitLab tool to merge branches instead of simply interacting with the Git server via command line. Merge requests should be easy to use and provide enough visible value such that users will default to use merge requests. This is a key objective of our effort to [restructure merge requests](https://gitlab.com/groups/gitlab-org/-/epics/5038).
- **Love-ability** captures the essence that GitLab is enjoyable to use, which may mean that it is fast, invisible and allows you to get your work done. Particularly, GitLab should encourage the best of communication between colleagues and contributors, helping teams celebrate great contributions of all kinds, and express their ideas without misunderstandings. How GitLab communicates with people, will influence how people communicate with each other inside GitLab. Utilizing [expressive merge request comments](https://gitlab.com/groups/gitlab-org/-/epics/4349) allows this communication to happen in a clear and structured way for all participants.
- **Efficiency** directly influences velocity within the time span of a single merge request
    - *Author efficiency* considers how a merge request author can create and address code review feedback, find a relevant reviewer (with [suggested reviewers](https://docs.gitlab.com/ee/user/project/merge_requests/reviews/#suggested-reviewers)) for their merge request, and incorporate incoming feedback.
    - *Reviewer efficiency* considers how an individual reviewer can review a code change, leave feedback, and also verify their own feedback has been addressed. Provide enhanced context when reviewing new information (for example, through code intelligence) for efficiency. We're currently working on review efficiency through [improvements to the review comments experience](https://gitlab.com/groups/gitlab-org/-/epics/7362).
    - *Team efficiency* considers a team can coordinate and communicate responsibilities, progress and status of a merge request, and quickly the entire process can be completed. Support workflows that enable new and better ways of working (for example, suggest changes, commit by commit review).
- **Best practices** Influence efficiency of teams and projects over a longer time scale, and can include fostering norms and behaviours that aren't explicitly enforced through the application. Amplifying best practices, great defaults and documentation play a significant role in this.
- **Policy** controls that allows code review requirements to be set and enforced, going above and beyond amplifying and encouraging best practice. We're partnering with the Compliance team to help [bring existing controls up to higher levels](https://gitlab.com/groups/gitlab-org/-/epics/4367) to make compliance easier across your organization.

## 1 year plan
<!--
1 year plan for what we will be working on linked to up-to-date epics. This section will be most similar to a "road-map". Items in this section should be linked to issues or epics that are up to date. Indicate relative priority of initiatives in this section so that the audience understands the sequence in which you intend to work on them. 
 -->

**Feature Enhancements**

- **In Progress:** [Real-time merge request updates](https://gitlab.com/groups/gitlab-org/-/epics/1812)

    While viewing a merge request, changes can be made to the merge request via pushes, approvals/comments, other users interacting with the merge request, pipelines completing and many other actions. These changes can impact the state of the merge request or the information that you need to review. Ensuring that actions update the merge request in areas like approvals, merge widget, and changes will build confidence with the merge request.

- **Later:**  [Expressive merge request comments](https://gitlab.com/groups/gitlab-org/-/epics/4349)

    Communicating the intent behind each comment left during a review is important for making it clear what needs to be addressed to make the improvement, and what can be addressed in future iterations. One way to solve this is with [conventional comments](https://gitlab.com/gitlab-org/gitlab/-/issues/216486) to correctly classifiy the feedback.

**Performance & Reliability Improvements**

- **In Progress:** [Merge request performance roundtables](https://gitlab.com/groups/gitlab-org/-/epics/9086)

    The performance of the merge request is critical in moving our product forward and improving developer satisfaction with the product. In an effort to take a modern approach with the engineering tools available to us today, we're beginning a series of performance roundtables focused on improving the experience from a core level.

**Research & Design Efforts**

- **In Progress:** [Restructure merge requests](https://gitlab.com/groups/gitlab-org/-/epics/5038)

    The merge request interface is overwhelming and can be challenging to find the information you need to complete the tasks you're working on. We're exploring several areas to restructure information on the page, remove items where appropriate, and increase the focus on completing the important tasks for merge requests. 

**In Progress:** [Review Rounds](https://gitlab.com/groups/gitlab-org/-/epics/9577)

    Code reviews typically involve multiple rounds of review as feedback is given, iterations made, and re-reviews happen. Understanding which merge requests a user needs to act on and the role they're playing in the merge request is an important part of communicating both state and responsiblity. We're looking in to more structured rounds of reviews to better inform users not only of what action is needed, but also when that action is needed.

### What is next for us
<!-- This is a 3 month look ahead for the next iteration that you have planned for the category. This section must provide links to issues or
or to [epics](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) that are scoped to a single iteration. Please do not link to giant epics that lack clarity on what is next. -->

- [Real-time merge widget](https://gitlab.com/groups/gitlab-org/-/epics/8639) - we'll continue to add triggers for the merge widget to make sure actions update it appropriately as we work to roll out the final pieces of this change.

### What we are currently working on
<!-- Scoped to the current month. This section can contain the items that you choose to highlight on the kickoff call. Only link to issues, not Epics.  -->

The Code Review Group continues to focus on providing [real-time merge request updates](https://gitlab.com/groups/gitlab-org/-/epics/1812). Our current focus is on improving [real-time updates for the merge button widget](https://gitlab.com/groups/gitlab-org/-/epics/8639): 

 - [Trigger mergeRequestMergeStatusUpdated when a blocking MR is added or removed](https://gitlab.com/gitlab-org/gitlab/-/issues/372522) - 15.9
 - [Trigger mergeRequestMergeStatusUpdated when a blocking MR gets merged ](https://gitlab.com/gitlab-org/gitlab/-/issues/372642) - 15.9

**Research & Design Efforts**

 - [Review Rounds design concepts](https://gitlab.com/gitlab-org/gitlab/-/issues/387222) - continue exploring the merge request state and how we can communicate review cycles more easily to merge request participants.

### What we recently completed
<!-- Lookback limited to 3 months. -->

Code Review has been focused on efforts related to [real-time merge request updates](https://gitlab.com/groups/gitlab-org/-/epics/1812) and has recently delivered:

 - [Trigger mergeRequestMergeStatusUpdated when a new unresolved discussion is started](https://gitlab.com/gitlab-org/gitlab/-/issues/372643) - 15.8
 - [Trigger mergeRequestMergeStatusUpdated when discussion is marked or unmarked as resolved](https://gitlab.com/gitlab-org/gitlab/-/issues/372523) - 15.8
 - [Update merge request widget component to use the GraphQL subscription](https://gitlab.com/gitlab-org/gitlab/-/issues/372515) - 15.7
 - [Trigger mergeRequestMergeStatusUpdated when all approvals are reset](https://gitlab.com/gitlab-org/gitlab/-/issues/372634) - 15.6
 - [Trigger mergeRequestMergeStatusUpdated when MR is approved or unapproved](https://gitlab.com/gitlab-org/gitlab/-/issues/372520) - 15.6
 - [Trigger mergeRequestMergeStatusUpdated when MR is closed](https://gitlab.com/gitlab-org/gitlab/-/issues/372519) - 15.6
 - [Update reviewer component to use the GraphQL subscription](https://gitlab.com/gitlab-org/gitlab/-/issues/366860) - 15.6

We've also delivered on important updates that finished off work our work to [better define mergeability](https://gitlab.com/groups/gitlab-org/-/epics/5598) in merge requests:

 - [Introduce new detailed_merge_status to the API](https://gitlab.com/gitlab-org/gitlab/-/issues/370856) - 15.6
 - [Introduce the detailed_merge_status to the web hooks](https://gitlab.com/gitlab-org/gitlab/-/issues/370857) - 15.6

Other important issues recently delivered by the group can be seen in [this list](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=closed_at_desc&state=closed&label_name%5B%5D=group%3A%3Acode%20review&label_name%5B%5D=direction&first_page_size=100).

### What is Not Planned Right Now
<!--  Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand -->

- [Cross-project code review (group merge requests)](https://gitlab.com/groups/gitlab-org/-/epics/882): Is not something the group is currently focused on solving. While we recognize that this is a workflow some teams require we're currently focused on improving support for existing review workflows within GitLab.
- [Commit by commit code review](https://gitlab.com/groups/gitlab-org/-/epics/285): Is not the primary code review flow within GitLab that we're currently optimizing for. We understand that some users prefer to review code this way, and there is basic tooling to support this workflow, but we're focused on our primary review process within GitLab.
- [Block merge request with a negative approval signal](https://gitlab.com/gitlab-org/gitlab/-/issues/761): Having a single way to block merge requests is not inline with how we view the review process. We'd like to continue to explore [expressive merge request comments](https://gitlab.com/groups/gitlab-org/-/epics/4349) as a way to further classify types of threads and potentially extend it with [restricting roles that can resolve threads](https://gitlab.com/gitlab-org/gitlab/-/issues/343479) to achieve similar controls.

## Best in Class Landscape 
<!-- Summary of the competitive landscape for top 3 competitors. Identification of the competitor which we consider to be "Best in Class" and why. Link to epics/issues that would close the gaps between and us and that competitor. 

(For non-marketing categories this section is optional) 
-->

GitLab competes with both dedicated and integrated code review tools. Because merge requests (which is the code review interface), and more specifically the merge widget, is the single source of truth about a code change and a critical control point in the GitLab workflow, it is important that merge requests and code review in GitLab is excellent. Our primary source of competition and comparison is to **dedicated code review tools**. See the [best in class analysis](https://internal-handbook.gitlab.io/handbook/product/best-in-class/create/#code-review) for an overview. For a closer look at the user experience and feature set of competitor tools see [these details](https://internal-handbook.gitlab.io/handbook/product/best-in-class/create/code_review_workflow/). (Both links are internal only).

## Maturity Plan
<!-- It's important your users know where you're headed next. The maturity plan section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/direction/maturity/)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) 

(For non-marketing categories this section is optional)  -->

This category is currently at the **Loveable** maturity level (see our [definitions of maturity levels](https://about.gitlab.com/direction/maturity/)).

## Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->

Code review is used by software engineers and individual contributors of all kinds. Depending on their context, however, the workflow and experience of code review can vary significantly.

- **full time contributor** to a commercial product where reducing cycle time is important. The review cycle is tight and focussed as a consequence of best practices where keeping merge requests small and iterating at a high velocity are objectives. Code review workflows for these users are **Complete**
- **occasional contributor** to an open source product where cycle time is typically longer as a consequence that they are not working on the project full time. This results in longer review times. When long review times occur, the participants in the merge request will need to spend more time reacquainting themselves with the change. When there are non-trivial amounts of feedback this can be difficult to understand. Code review workflows for these users are **Complete**
- **scientific projects** frequently have a different flow to typical projects, where the development is sporadic, and changes are often reviewed after they have been merged to master. This is a consequence of the high code churn associated with high exploratory work, and having infrequent access to potential reviewers. Post-merge code review workflows are not yet viable in GitLab.

We primarily focus research efforts around [Sasha (Software Developer)](/handbook/product/personas/#sasha-software-developer).
